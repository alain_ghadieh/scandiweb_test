# Scandiweb Junior Developer Test #

Scandiweb PHP OOP test task.

### Setup ###

* Import database schema provided (also includes some table records).
* The project includes two pages: 
    1. product list, where one can also delete multiple products at once
    2. add new product form
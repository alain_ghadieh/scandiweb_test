<?php

require_once './autoload.php';
require_once './public/layout.php';
$database = new Scandiweb\Library\Database();
$res = $database->read();
?>

<head>
    <title>Product List</title>
</head>
<body>

<main>
    <form method="POST" action="Scandiweb/Library/delete.php">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="align-middle ml-5">
                <h1 class="poppins display-5">Product List</h1>
            </div>

            <div class="form-inline my-2 my-lg-0 ml-auto mr-5">
                <button class="lead btn btn-outline-primary mr-3" type="button"
                        onclick="document.location.href = 'add-product'">ADD
                </button>
                <button id="delete-product-btn" type="submit"
                        class="lead btn btn-outline-danger pull-right my-2 my-sm-0">MASS DELETE
                </button>
            </div>
        </nav>

        <div class="roboto-condensed container">
            <br>
            <div class="row row-cols-md-4">
                <?php
                foreach ($res as $item) {
                    $number_format = number_format(floatval($item->price), 2);
                    print <<<HTHT
                        <div class="col mb-4">
                            <div class="card" >
                                <div class="card-body">
                                    <input type="checkbox" name="delete[]" class="delete-checkbox" value=$item->id>
                                    <p class="card-text text-center">$item->sku</p>
                                    <p class="card-text text-center">$item->name</p>
                                    <p class="card-text text-center">$ $number_format</p>
                                    <p class="card-text text-center">{$item->getSpecialAttribute()}</p>
                                </div>
                            </div>
                        </div>
    HTHT;
                }
                ?>
            </div>
        </div>
    </form>
</main>

<footer class="footer">
    <div class="bg-light poppins">
        <p class="text-center text-muted">Scandiweb Test assignment</p>
    </div>
</footer>
</body>
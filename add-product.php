<?php
require_once './autoload.php';
require_once './public/layout.php';
?>
<head>
    <title>Product Add</title>
</head>
<body>
<main>
    <form id="product_form" method="POST" action="Scandiweb/Library/add.php" onsubmit="return validate();">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="align-middle ml-5">
                <h1 class="poppins display-5">Product Add</h1>
            </div>

            <div class="form-inline my-2 my-lg-0 ml-auto mr-5">
                <button id="save-btn" class="lead btn btn-outline-primary mr-3" type="submit">Save</button>
                <button type="button" onclick="document.location.href = 'index'"
                        class="lead btn btn-outline-danger pull-right my-2 my-sm-0">Cancel
                </button>
            </div>
        </nav>
        <br>
        <div class="container">
            <div id="notification"></div>
            <div class="form-group row">
                <label for="sku" class="col-sm-2 col-form-label">SKU</label>
                <div class="col-sm-4">
                    <input type="text" id="sku" name="sku" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label for="name" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-4">
                    <input type="text" id="name" name="name" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label for="price" class="col-sm-2 col-form-label">Price ($)</label>
                <div class="col-sm-4">
                    <input type="number" id="price" name="price" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label for="productType" class="col-sm-2 col-form-label">Type Switcher</label>
                <div class="col-sm-3">
                    <select id="productType" name="productType" class="form-control">
                        <option value="">Type Switcher</option>
                        <option value="Dvd">DVD</option>
                        <option value="Book">Book</option>
                        <option value="Furniture">Furniture</option>
                    </select>
                </div>
            </div>
            <div id="specialAttributes"></div>
        </div>
    </form>
</main>

<footer class="footer">
    <div class="bg-light poppins">
        <p class="text-center text-muted">Scandiweb Test assignment</p>
    </div>
</footer>
</body>
<script>
    $("#productType").change(function () {
        let productType = $("#productType").val();
        let elem = $('#specialAttributes').empty();
        if (productType === "Dvd") {
            elem.append(
                `<div class="form-group row">
                <label for="size" class="col-sm-2 col-form-label">Size (MB)</label>
                <div class="col-sm-4">
                    <input type="number" id="size" name="size" class="form-control" >
                </div>
            </div>
            <small>Please, provide size</small>
            `
            );
        } else if (productType === "Book") {
            elem.append(
                `<div class="form-group row">
                <label for="weight" class="col-sm-2 col-form-label">Weight (KG)</label>
                <div class="col-sm-4">
                    <input type="number" id="weight" name="weight" class="form-control" >
                </div>
            </div>
            <small>Please, provide weight</small>
            `
            );
        } else if (productType === "Furniture") {
            elem.append(
                `<div class="form-group row">
                <label for="height" class="col-sm-2 col-form-label">Height (CM)</label>
                <div class="col-sm-4">
                    <input type="number" id="height" name="height" class="form-control" >
                </div>
            </div>
            <div class="form-group row">
                <label for="width" class="col-sm-2 col-form-label">Width (CM)</label>
                <div class="col-sm-4">
                    <input type="number" id="width" name="width" class="form-control" >
                </div>
            </div>
            <div class="form-group row">
                <label for="length" class="col-sm-2 col-form-label">Length (CM)</label>
                <div class="col-sm-4">
                    <input type="number" id="length" name="length" class="form-control" >
                </div>
            </div>
            <small>Please, provide dimensions</small>
            `
            );
        }
    });

    function validate() {
        let elem = $('#notification').empty();

        let fields = $("#product_form")
            .find("select, textarea, input")
            .serializeArray();

        let numberFields = $("#product_form")
            .find('input[type="number"]')
            .serializeArray();
        console.log(numberFields);
        for (let i = 0; i < fields.length; i++) {
            if (!fields[i].value) {
                elem.append(
                    `<div class="alert alert-danger" role="alert">
                Please, submit required data
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            `);
                return false;
            }
        }
        for (let i = 0; i < numberFields.length; i++) {
            if (numberFields[i].value < 1) {
                elem.append(
                    `<div class="alert alert-danger" role="alert">
                Please, provide the data of indicated type
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            `);
                return false;
            }
        }

        return true;
    }
</script>

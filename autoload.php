<?php

function myautoload($className)
{
    $path = __DIR__ . '/' . str_replace('\\', '/', $className) . '.php';
    require_once($path);
}

spl_autoload_register('myautoload');

<?php

namespace Scandiweb\Library;

require_once 'config.php';

class Database
{
    private $connection;

    function __construct()
    {
        $this->connectDb();
    }

    public function connectDb()
    {
        $this->connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        if (mysqli_connect_error()) {
            die("Database Connection Failed" . mysqli_connect_error() . mysqli_connect_errno());
        }
    }

    public function read(): array
    {
        $sql = "SELECT * FROM `product`";
        $res = mysqli_query($this->connection, $sql);
        $all = array();
        while ($item = mysqli_fetch_assoc($res)) {
            $className = "Scandiweb\\Model\\" . $item['type'];
            $obj = new $className($item);
            array_push($all, $obj);
        }
        return $all;
    }

    public function create($item): bool
    {
        $className = "Scandiweb\\Model\\" . $item['type'];
        $obj = new $className($item);
        $res = mysqli_query($this->connection, $obj->sqlStatement());
        if ($res) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($id): bool
    {
        $sql = "DELETE FROM `product` WHERE id=$id";
        $res = mysqli_query($this->connection, $sql);
        if ($res) {
            return true;
        } else {
            return false;
        }
    }
}

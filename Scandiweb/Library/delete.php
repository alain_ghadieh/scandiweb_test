<?php

use  Scandiweb\Library\Database;

require_once '../../autoload.php';

if (isset($_POST['delete'])) {
    $database = new Database();
    $productIds = $_POST['delete'] ?? null;
    $res = false;
    foreach ($productIds as $id) {
        $res = $database->delete($id);
    }
    if ($res) {
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    } else {
        echo "Failed to Delete Record";
    }
}
<?php

use Scandiweb\Library\Database;

require_once '../../autoload.php';

$item = array();

$item['sku'] = $_POST['sku'] ?? null;
$item['name'] = $_POST['name'] ?? null;
$item['price'] = $_POST['price'] ?? null;
$item['type'] = $_POST['productType'] ?? null;
$item['size'] = $_POST['size'] ?? null;
$item['weight'] = $_POST['weight'] ?? null;
$item['height'] = $_POST['height'] ?? null;
$item['width'] = $_POST['width'] ?? null;
$item['length'] = $_POST['length'] ?? null;

$database = new Database();
$res = $database->create($item);
header('Location: ../../index.php');

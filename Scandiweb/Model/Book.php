<?php

namespace Scandiweb\Model;

class Book extends Product
{
    public $weight;

    function __construct($item)
    {
        $this->weight = $item['weight'];
        parent::__construct($item);
    }

    public function getSpecialAttribute(): string
    {
        return 'Weight: ' . $this->weight . 'KG';
    }

    public function sqlStatement(): string
    {
        return "INSERT INTO product (sku, name, price, type, weight) VALUES ('" . $this->sku . "', '" . $this->name . "', " . $this->price . ", '" . $this->type . "', " . $this->weight . ")";
    }
}
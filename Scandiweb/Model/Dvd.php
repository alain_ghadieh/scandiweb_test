<?php

namespace Scandiweb\Model;

class Dvd extends Product
{
    public $size;

    function __construct($item)
    {
        $this->size = $item['size'];
        parent::__construct($item);
    }

    public function getSpecialAttribute(): string
    {
        return 'Size: ' . $this->size . 'MB';
    }

    public function sqlStatement(): string
    {
        return "INSERT INTO product (sku, name, price, type, size) VALUES ('" . $this->sku . "', '" . $this->name . "', " . $this->price . ", '" . $this->type . "', " . $this->size . ")";
    }
}

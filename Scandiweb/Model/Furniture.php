<?php

namespace Scandiweb\Model;

class Furniture extends Product
{
    public $height;
    public $width;
    public $length;

    function __construct($item)
    {
        $this->height = $item['height'];
        $this->width = $item['width'];
        $this->length = $item['length'];
        parent::__construct($item);
    }

    public function getSpecialAttribute(): string
    {
        return 'Dimensions: ' . $this->height . 'x' . $this->width . 'x' . $this->length;
    }

    public function sqlStatement(): string
    {
        return "INSERT INTO product (sku, name, price, type, height, width, length) VALUES ('" . $this->sku . "', '" . $this->name . "', " . $this->price . ", '" . $this->type . "', " . $this->height . ", " . $this->width . " , " . $this->length . ")";
    }
}
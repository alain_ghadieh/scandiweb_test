<?php

namespace Scandiweb\Model;

class Product
{
    public $id;
    public $sku;
    public $name;
    public $price;
    public $type;

    function __construct($item)
    {
        $this->id = $item['id'];
        $this->sku = $item['sku'];
        $this->name = $item['name'];
        $this->price = $item['price'];
        $this->type = $item['type'];
    }

    public function getSpecialAttribute(): string
    {
        return '';
    }
}

